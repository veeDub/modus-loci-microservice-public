-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: internal-db.s32175.gridserver.com
-- Generation Time: Feb 26, 2021 at 05:10 AM
-- Server version: 5.6.31-77.0
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db32175_mindMapper_School`
--

-- --------------------------------------------------------

--
-- Table structure for table `allContent`
--

CREATE TABLE `allContent` (
  `id` int(11) NOT NULL,
  `tID` varchar(256) NOT NULL DEFAULT '',
  `type` varchar(15) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  `heading` varchar(255) NOT NULL DEFAULT '',
  `note` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(256) NOT NULL DEFAULT '',
  `grokLevel` varbinary(256) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `allSources`
--

CREATE TABLE `allSources` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(15) NOT NULL DEFAULT '',
  `tID` varchar(256) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `citation` varchar(60) NOT NULL,
  `abstract` text NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `tID` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `tID` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `tID` varchar(255) NOT NULL,
  `type` varchar(15) NOT NULL,
  `class` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `priority` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keywords` varchar(255) NOT NULL,
  `source` varchar(256) NOT NULL,
  `grokLevel` int(125) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE `lectures` (
  `id` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `tID` varchar(256) NOT NULL,
  `class` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `myp`
--

CREATE TABLE `myp` (
  `id` int(11) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paperContent`
--

CREATE TABLE `paperContent` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `paperID` int(11) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `firstDraft` text NOT NULL,
  `finalDraft` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paperInfo`
--

CREATE TABLE `paperInfo` (
  `id` int(11) NOT NULL,
  `tID` varchar(256) NOT NULL,
  `class` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sections` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `argument` text NOT NULL,
  `evidence` text NOT NULL,
  `citations` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paperSpine`
--

CREATE TABLE `paperSpine` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sectionTitle` varchar(100) NOT NULL,
  `paperID` int(20) NOT NULL,
  `sectionID` int(11) NOT NULL,
  `sectionArgument` text NOT NULL,
  `sectionEvidence` text NOT NULL,
  `sectionWordCount` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `tID` varchar(256) NOT NULL,
  `type` varchar(15) NOT NULL,
  `class` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `priority` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keywords` varchar(255) NOT NULL,
  `source` varchar(256) NOT NULL,
  `grokLevel` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `tID` varchar(11) NOT NULL,
  `class` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `abstract` mediumblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `tID` varchar(256) NOT NULL,
  `type` varchar(20) NOT NULL,
  `class` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `file` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE `time` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `source` varchar(255) NOT NULL,
  `sourceTimeLength` int(11) NOT NULL,
  `sourceWordLength` int(11) NOT NULL,
  `taskVar` varchar(11) NOT NULL,
  `tID` varchar(11) NOT NULL,
  `cID` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `symbols` varchar(350) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allContent`
--
ALTER TABLE `allContent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `allSources`
--
ALTER TABLE `allSources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `myp`
--
ALTER TABLE `myp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paperContent`
--
ALTER TABLE `paperContent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paperInfo`
--
ALTER TABLE `paperInfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paperSpine`
--
ALTER TABLE `paperSpine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allContent`
--
ALTER TABLE `allContent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `allSources`
--
ALTER TABLE `allSources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paperContent`
--
ALTER TABLE `paperContent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paperInfo`
--
ALTER TABLE `paperInfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paperSpine`
--
ALTER TABLE `paperSpine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `time`
--
ALTER TABLE `time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `topics` 
  CHANGE `symbols` `symbols` VARCHAR(350) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;