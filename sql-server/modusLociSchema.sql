SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
-- time_zone should prolly be set programatically once in production
SET time_zone = 'america/vancouver';

-- create topics table: symbols will be implemented later
CREATE TABLE topics (
  id int(11) NOT NULL AUTO_INCREMENT,
  topic varchar(255) NOT NULL UNIQUE,
  PRIMARY KEY (id)
); 

-- create sources table
CREATE TABLE classes (
  id int(11) NOT NULL AUTO_INCREMENT,
  tID int(11) NOT NULL,
  class varchar(255) NOT NULL UNIQUE,
  PRIMARY KEY (id),
  FOREIGN KEY (tID) 
    REFERENCES topics(id)
    ON DELETE CASCADE
); 


CREATE TABLE sources(
    id int(11) NOT NULL AUTO_INCREMENT,
    date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    type varchar(15) NOT NULL,
    tID int(11) NOT NULL,
    class int(11) NOT NULL,
    source varchar(255) NOT NULL UNIQUE,
    file varchar(255) DEFAULT '',
    author varchar(255) DEFAULT '',
    citation varchar(60) ,
    abstract text ,
    year int(4) ,
    PRIMARY KEY (id),
    FOREIGN KEY (tID) 
      REFERENCES topics(id)
      ON DELETE CASCADE,
    FOREIGN KEY (class) 
      REFERENCES classes(id)
      ON DELETE CASCADE
);