package contracts

// db schema structs
type Topic struct {
	Id       int `json:",omitempty"`
	Topic    string
	TopicNew string `json:",omitempty"`
}

type Class struct {
	Id       int `json:",omitempty"`
	TId      string
	Class    string
	ClassNew string `json:",omitempty`
}

type Source struct {
	Id       int    `json:",omitempty"`
	Type     string `json:",omitempty"`
	TId      int    `json:",omitempty"`
	Class    int    `json:",omitempty"`
	Source   string `json:",omitempty"`
	File     string `json:",omitempty"`
	Author   string `json:",omitempty"`
	Abstract string `json:",omitempty"`
}

type SourceEdit struct {
	Source string
	Edits  []SourceEdits
}

type SourceEdits struct {
	Key   string
	Value string
}
