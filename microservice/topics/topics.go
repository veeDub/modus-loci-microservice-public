package topics

import (
	// "database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	connection "modus-loci-microservice/connection"
	contracts "modus-loci-microservice/contracts"

	_ "github.com/go-sql-driver/mysql"
)

// read topics
func GetTopics(w http.ResponseWriter, r *http.Request) {
	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "SELECT id, topic FROM topics"
	topics, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	allTopics := make([]contracts.Topic, 0)

	for topics.Next() {
		var topic contracts.Topic
		if err := topics.Scan(&topic.Id, &topic.Topic); err != nil {
			fmt.Println(err)
		}
		allTopics = append(allTopics, topic)
	}
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer topics.Close()

	answer, err := json.Marshal(allTopics)
	w.Write(answer)
}

// create topic
// { "topic" : "PSYCH" } // just topic name needed
func CreateTopic(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var topic contracts.Topic

	err = json.Unmarshal(body, &topic)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "INSERT INTO `topics`(`topic`) VALUES ('" + topic.Topic + "')"
	insert, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer insert.Close()
	fmt.Fprintf(w, "Topic %s has been added.", topic.Topic)

}

// update topic
// { "topic" : "PSYCHS", "topicNew" : "PSYCH"}
func EditTopic(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var topic contracts.Topic
	err = json.Unmarshal(body, &topic)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "UPDATE `topics` SET topic= \"" + topic.TopicNew + "\" WHERE topic = \"" + topic.Topic + "\""

	update, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}
	defer update.Close()

	fmt.Fprintf(w, "Topic %s has been edited to %s.", topic.Topic, topic.TopicNew)
}

// delete topic
// { "topic" : "PHIL" }
func DeleteTopic(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var topic contracts.Topic
	err = json.Unmarshal(body, &topic)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "DELETE FROM `topics` WHERE topic = \"" + topic.Topic + "\""

	delete, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer delete.Close()

	fmt.Fprintf(w, "Topic %s has been deleted.", topic.Topic)
}
