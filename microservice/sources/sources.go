package sources

import (

	// "database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	connection "modus-loci-microservice/connection"
	contracts "modus-loci-microservice/contracts"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

// read sources
func GetSources(w http.ResponseWriter, r *http.Request) {
	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "SELECT id, type, tID, class, source, file, author, abstract FROM sources"
	sources, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	allSources := make([]contracts.Source, 0)
	for sources.Next() {
		var source contracts.Source
		if err := sources.Scan(&source.Id, &source.Type, &source.TId, &source.Class, &source.Source, &source.File, &source.Author, &source.Abstract); err != nil {
			connection.ReturnError(w, err, http.StatusBadGateway)
			return
		}
		allSources = append(allSources, source)
	}
	if err := sources.Err(); err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}
	defer sources.Close()

	answer, err := json.Marshal(allSources)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	w.Write(answer)
}

// create source
func CreateSource(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	var source contracts.Source
	err = json.Unmarshal(body, &source)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := fmt.Sprintf("INSERT INTO `sources` (`type`, `tID`, `class`, `source`, `file`, `author`, `abstract`) VALUES (\"%s\", %d, %d, \"%s\", \"%s\", \"%s\", \"%s\")", source.Type, source.TId, source.Class, source.Source, source.File, source.Author, source.Abstract)

	insert, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}
	defer insert.Close()

	fmt.Fprintf(w, "Source Added: %s", source.Source)

}

// update source
// { "source" : "Name of the Source", "edits" : [{"key":"file", "value":"NewFileName"}, {"key":"author", "value":"Edtied Author"}]}
func EditSource(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	var source contracts.SourceEdit
	err = json.Unmarshal(body, &source)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	queryString := "UPDATE `sources` SET "
	for i := 0; i < len(source.Edits); i++ {
		queryString += fmt.Sprint("`", source.Edits[i].Key, "` = \"", source.Edits[i].Value, "\"")
		if i < len(source.Edits)-1 {
			queryString += ", "
		}
	}
	queryString += " WHERE source = \"" + source.Source + "\""

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	update, err := db.Query(queryString)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}
	defer update.Close()

	fmt.Fprintf(w, "Source %s Edited.", source.Source)
}

// delete source
func DeleteSource(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var source contracts.Source
	err = json.Unmarshal(body, &source)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	myQuery := "DELETE FROM `sources` WHERE source = \"" + source.Source + "\""

	delete, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}
	defer delete.Close()

	fmt.Fprintf(w, "Source %s Deleted.", source.Source)
}
