package main

import (
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"

	classes "modus-loci-microservice/classes"
	connection "modus-loci-microservice/connection"
	sources "modus-loci-microservice/sources"
	topics "modus-loci-microservice/topics"
)

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome home")
}

func testDB(w http.ResponseWriter, r *http.Request) {
	db, err := connection.InitDB()
	if err != nil {
		fmt.Println("Ping error: ", err)
	}
	db.Ping()
	fmt.Fprintf(w, "Ping has ponged.")
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", homeLink)
	router.HandleFunc("/testDB", testDB).Methods("GET")

	router.HandleFunc("/topics", topics.GetTopics).Methods("GET")
	router.HandleFunc("/topic", topics.CreateTopic).Methods("POST")
	router.HandleFunc("/topic", topics.EditTopic).Methods("PUT")
	router.HandleFunc("/topic", topics.DeleteTopic).Methods("DELETE")

	router.HandleFunc("/classes", classes.GetClasses).Methods("GET")
	router.HandleFunc("/classes/topic", classes.GetClassByTopic).Methods("GET")
	router.HandleFunc("/class", classes.CreateClass).Methods("POST")
	router.HandleFunc("/class", classes.EditClass).Methods("PUT")
	router.HandleFunc("/class", classes.DeleteClass).Methods("DELETE")

	router.HandleFunc("/sources", sources.GetSources).Methods("GET")
	router.HandleFunc("/source", sources.CreateSource).Methods("POST")
	router.HandleFunc("/source", sources.EditSource).Methods("PUT")
	router.HandleFunc("/source", sources.DeleteSource).Methods("DELETE")

	go log.Fatal(http.ListenAndServe(":3001", router))
}
