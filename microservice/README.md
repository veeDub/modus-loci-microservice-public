<!-- NAME OF SQL DATABASE BOTH LOCAL & REMOTE -->
ModusLociSchool

<!-- build & run commands for golang service -->
docker build -t veewyns/modusloci-service .
docker container run -d --name service -p 3001:3001 veewyns/modusloci-service
<!-- push to dockerhub -->
docker push veewyns/modusloci-service
docker pull veewyns/modusloci-service

<!-- getting db schema to vm at the root level-->
scp db32175_mindMapper_School.sql root@138.197.232.38:/


<!-- mysql docker container for actual database-->
docker pull mysql/mysql-server
docker run --name=mysql1 --restart on-failure -d mysql/mysql-server

<!-- once the docker sql instance is "healthy" get password -->
<!-- see if healthy with docker ps usually takes ~ a minute -->
docker logs mysql1 2>&1 | grep GENERATED

<!-- ssh into container -->
docker exec -it mysql1 mysql -uroot -p
<!-- set the password -->
ALTER user 'root'@'localhost' IDENTIFIED BY 'modusLoci2021*';  

<!-- to control the mysql server exit out of container -->
docker start mysql1
docker restart mysql1
docker stop mysql1

<!-- set up database schema -->
docker cp db32175_mindMapper_School.sql mysql1:/
<!-- get the container id -->
docker ps 
<!-- then ssh into container with id -->
docker exec -it [containerID] /bin/bash
<!-- now import the sql schema into database -->
source /db32175_mindMapper_School.sql;

<!-- // CI/CD gitlab
# ssh-keyscan gitlab.com >> authorized_keys:  Run on server terminal, then save in a gitlab variable
# cat id_rsa.pub >> authorized_keys Run this command on the server on the terminal. Make sure that you are logged in as 'deployer'
# Both COMMANDS ABOVE ARE necessary. -->