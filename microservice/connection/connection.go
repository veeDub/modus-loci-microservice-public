package connection

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Create an exported global variable to hold the database connection pool.
var DB *sql.DB

func InitDB() (*sql.DB, error) {
	pwd, err := ioutil.ReadFile("/run/secrets/db-password")
	connection_string := fmt.Sprintf("root:%s@tcp(127.0.0.1:3306)/ModusLoci", string(pwd))

	if err != nil {
		return nil, err
	}

	return sql.Open("mysql", connection_string)
}

func ReturnError(w http.ResponseWriter, err error, status int) {
	myError := err.Error()
	w.WriteHeader(status)
	fmt.Fprintf(w, myError)
}
