package classes

import (
	// "database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	connection "modus-loci-microservice/connection"
	contracts "modus-loci-microservice/contracts"

	_ "github.com/go-sql-driver/mysql"
)

// read classes
func GetClasses(w http.ResponseWriter, r *http.Request) {
	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "SELECT id, tID, class FROM classes"

	classes, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	allClasses := make([]contracts.Class, 0)
	for classes.Next() {
		var class contracts.Class
		if err := classes.Scan(&class.Id, &class.TId, &class.Class); err != nil {
			connection.ReturnError(w, err, http.StatusBadRequest)
			return
		}
		allClasses = append(allClasses, class)
	}
	if err := classes.Err(); err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	defer classes.Close()

	answer, err := json.Marshal(allClasses)

	w.Write(answer)
}

// get a specific class
func GetClassByTopic(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var class contracts.Class
	err = json.Unmarshal(body, &class)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := fmt.Sprintf("SELECT id, tID, class FROM classes WHERE tID = \"%s\"", class.TId)
	classes, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	allClasses := make([]contracts.Class, 0)
	for classes.Next() {
		var class contracts.Class
		if err := classes.Scan(&class.Id, &class.TId, &class.Class); err != nil {
			connection.ReturnError(w, err, http.StatusInternalServerError)
			return
		}
		allClasses = append(allClasses, class)
	}
	if err := classes.Err(); err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	defer classes.Close()

	answer, err := json.Marshal(allClasses)
	w.Write(answer)
}

// create classes
// {"TId":"5","Class":"PSYCH365"}
func CreateClass(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var class contracts.Class
	err = json.Unmarshal(body, &class)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()

	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "INSERT INTO `classes`(`tID`, `class`) VALUES ('" + class.TId + "', '" + class.Class + "')"

	insert, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer insert.Close()
	fmt.Fprintf(w, "Class %s has been added.", class.Class)
}

// update class
// { "class" : "PSYCH365", "classNew" : "PSYCH402"}
func EditClass(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var class contracts.Class
	err = json.Unmarshal(body, &class)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "UPDATE `classes` SET class= '" + class.ClassNew + "' WHERE class = \"" + class.Class + "\""
	update, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer update.Close()

	fmt.Fprintf(w, "Class %s has been edited to %s.", class.Class, class.ClassNew)
}

// delete class
// { "class" : "PSYCH402" }
func DeleteClass(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	var class contracts.Class
	err = json.Unmarshal(body, &class)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadRequest)
		return
	}

	db, err := connection.InitDB()
	defer db.Close()
	if err != nil {
		connection.ReturnError(w, err, http.StatusInternalServerError)
		return
	}

	myQuery := "DELETE FROM `classes` WHERE class = \"" + class.Class + "\""
	delete, err := db.Query(myQuery)
	if err != nil {
		connection.ReturnError(w, err, http.StatusBadGateway)
		return
	}

	defer delete.Close()
	fmt.Fprintf(w, "Class %s has been deleted.", class.Class)
}
