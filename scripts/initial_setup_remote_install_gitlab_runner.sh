# fresh VM has docker installed & and a user called vee that you need to ssh in as

# now install and register gitlab-runner
sudo curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh > script.deb.sh
sudo chmod +x script.deb.sh
sudo bash script.deb.sh
sudo apt install gitlab-runner
# systemctl status gitlab-runner
sudo gitlab-runner register -n --url https://gitlab.com --registration-token d9x6ENt6avoN5Q54QCix --executor shell --description "ModusLoci content microservice" --docker-image "docker:stable" --tag-list shell --docker-privileged
sudo gitlab-runner run

# sudo adduser deployer
# sudo usermod -aG docker deployer
# su deployer
# ssh-keygen -b 4096
# cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
# cat ~/.ssh/id_rsa


# now scp setup.sh in modus-loci/modus-loci folder structure
# mkdir modus-loci
# cd modus-loci
# mkdir modus-loci
# cd modus-loci

# scp file.txt remote_username@10.10.0.2:/remote/directory
# scp restart.sh deployer@164.90.247.34:/modus-loci/modus-loci

# sudo chmod +x scripts/restart.sh

# install docker compose
# sudo curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# sudo add-apt-repository \
# >    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
# >    $(lsb_release -cs) \
# >    stable"

# sudo apt-get update

# # add deployer to the docker group
# sudo groupadd docker
# sudo usermod -aG docker deployer

# # Login to docker
# docker login --username  --password 

# attach db volume
# Create a mount point for your volume:
# mkdir -p /mnt/modus_loci_db

# # Mount your volume at the newly-created mount point:
# mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_modus-loci-db /mnt/modus_loci_db

# # Change fstab so the volume will be mounted after a reboot
# echo '/dev/disk/by-id/scsi-0DO_Volume_modus-loci-db /mnt/modus_loci_db ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab